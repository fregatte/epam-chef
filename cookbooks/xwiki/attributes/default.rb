default['xwiki']['db']['port'] = '3306'
default['xwiki']['db']['host'] = '172.17.0.1'
default['xwiki']['db']['dir'] = '/var/xwiki/mysql'
default['xwiki']['db']['root_pass'] = 'password'
default['xwiki']['db']['user_name'] = 'user'
default['xwiki']['db']['user_pass'] = 'password'
default['xwiki']['db']['db_name'] = 'new_db'

default['xwiki']['app']['port'] = '8080'
default['xwiki']['app']['host'] = '172.17.0.1'
default['xwiki']['app']['dir'] = '/var/xwiki/appdata'

default['xwiki']['lb']['conf_dir'] = '/var/xwiki/lb'
default['xwiki']['lb']['conf_file'] = 'xwiki_lb.conf'
default['xwiki']['lb']['port'] = '80'
default['xwiki']['lb']['vhost'] = 'xwiki.local'
default['xwiki']['lb']['upstreams'] = ['172.17.0.1:8080']
