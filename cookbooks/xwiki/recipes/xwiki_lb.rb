directory "#{node['xwiki']['lb']['conf_dir']}" do
  recursive true
end

template "#{node['xwiki']['lb']['conf_dir']}/#{node['xwiki']['lb']['conf_file']}" do
  variables(
  'lb_port': node['xwiki']['lb']['port'],
  'lb_vhost': node['xwiki']['lb']['vhost'],
  'lb_upstreams': node['xwiki']['lb']['upstreams']
  )
  source "#{node['xwiki']['lb']['conf_file']}.erb"
  notifies :restart, 'docker_container[xwiki-lb]', :immediately
end

docker_service 'default' do
  action [:create, :start]
end

docker_image 'nginx' do
  action :pull
end

docker_container 'xwiki-lb' do
  repo 'nginx'
  port "#{node['xwiki']['lb']['port']}:80"
  volumes ["#{node['xwiki']['lb']['conf_dir']}/#{node['xwiki']['lb']['conf_file']}:/etc/nginx/conf.d/#{node['xwiki']['lb']['conf_file']}:ro"]
  action :run
end
