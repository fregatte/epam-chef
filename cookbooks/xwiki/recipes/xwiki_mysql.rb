docker_service 'default' do
  action [:create, :start]
end

docker_image 'mysql' do
  tag '5.7'
  action :pull
end

docker_container 'xwiki-mysql' do
  repo 'mysql'
  tag '5.7'
  port "#{node['xwiki']['db']['port']}:3306"
  volumes ["#{node['xwiki']['db']['dir']}:/var/lib/mysql"]
  env ["MYSQL_ROOT_PASSWORD=#{node['xwiki']['db']['root_pass']}",
       "MYSQL_USER=#{node['xwiki']['db']['user_name']}",
       "MYSQL_PASSWORD=#{node['xwiki']['db']['user_pass']}",
       "MYSQL_DATABASE=#{node['xwiki']['db']['db_name']}"]
  command '--character-set-server=utf8 --collation-server=utf8_bin --explicit-defaults-for-timestamp=1'
  action :run
end
