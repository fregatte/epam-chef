docker_service 'default' do
  action [:create, :start]
end

docker_image 'xwiki' do
  tag 'mysql-tomcat'
  action :pull
end

docker_container 'xwiki-app' do
  repo 'xwiki'
  tag 'mysql-tomcat'
  port "#{node['xwiki']['app']['port']}:8080"
  volumes ["#{node['xwiki']['app']['dir']}:/usr/local/xwiki"]
  env ["DB_USER=#{node['xwiki']['db']['user_name']}",
       "DB_PASSWORD=#{node['xwiki']['db']['user_pass']}",
       "DB_DATABASE=#{node['xwiki']['db']['db_name']}",
       "DB_HOST=#{node['xwiki']['db']['host']}",
       "DB_PORT=#{node['xwiki']['db']['port']}"]
  action :run
end
