property :instance_name, String, name_property: true
property :dir, String, required: true
property :hidden, [true, false], default: false

action :list_dir do
  execute 'cmd_ls' do
    if new_resource.hidden == true
      command "ls -la #{new_resource.dir}"
    else
      command "ls -l #{new_resource.dir}"
    end
    live_stream true
  end
end
